Command line instructions


Git global setup

git config --global user.name "Tushar Chowdhury"
git config --global user.email "tushar.chowdhury@gmail.com"

Create a new repository

git clone https://gitlab.com/tushar.chowdhury/Trainer_B68_Session3_Git.git
cd Trainer_B68_Session3_Git
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/tushar.chowdhury/Trainer_B68_Session3_Git.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/tushar.chowdhury/Trainer_B68_Session3_Git.git
git push -u origin --all
git push -u origin --tags